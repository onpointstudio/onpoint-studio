<?php

$finder = Symfony\CS\Finder\DefaultFinder::create()
    ->ignoreVCS(true)
    ->ignoreDotFiles(true)
    ->in('./wp-content/themes')
    ->exclude('library')
    ->name('*.php')
;

return Symfony\CS\Config\Config::create()
    ->level(Symfony\CS\FixerInterface::PSR2_LEVEL)
    ->finder($finder)
;
