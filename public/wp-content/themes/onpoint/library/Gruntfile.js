module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: [
          'src/js/main.js',
          'bower_components/jquery/dist/jquery.js',
          'bower_components/uikit/js/uikit.min.js',
          'bower_components/uikit/js/components/sticky.min.js',
          'src/js/**/*.js'
        ],
        dest: 'main.min.js'
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compact'
        },
        files: {
          'main.min.css': 'src/scss/main.scss'
        }
      }
    },
    watch: {
      files: ['src/js/**/*.js','src/scss/**/*.scss'],
      tasks: ['uglify','sass']
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');

  // Default task(s).
  grunt.registerTask('default', ['uglify','sass']);

};
