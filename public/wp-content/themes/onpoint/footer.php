
    <div class="footer">
        <div class="uk-container uk-container-center">
            <div class="uk-block uk-grid uk-grid-width-1-1 uk-grid-width-medium-1-3" data-uk-grid-margin>
                <div></div>
                <div>
                    <h2>Availability</h2>
                    <p><i class="uk-icon-calendar-check-o uk-icon-small uk-icon-color-blue"></i> Diary slots are available...</p>
                </div>
                <div class="uk-text-left-small">
                    <h2>Contact</h2>
                    <p><i class="uk-icon-envelope uk-icon-small uk-icon-color-blue"></i> <a href="mailto:simonhuggs@gmail.com" title="simonhuggs@gmail.com">simonhuggs@gmail.com</a></p>
                </div>
                <div class="uk-width-1-1 uk-text-center uk-margin uk-text-small">
                    <p>Copyright <?=date('Y');?> OnPoint Studio. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>

    <?php wp_footer(); ?>
</body>
</html>
