<?php


define('TEXT_DOMAIN','ONPOINT');

// require_once "_post_types.php";
require_once "Menu_Walker.php";

/**
 * Add resources to the theme on setup.
 * Initial styles, js, theme setting here.
 */
add_action('after_setup_theme', '__after_theme_setup');
function __after_theme_setup()
{
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');

    register_nav_menus(array(
        'primary' => __('Primary Navigation', TEXT_DOMAIN),
        // 'footer' => __('Footer Links', TEXT_DOMAIN),
    ));
}

add_action('wp_enqueue_scripts', '__enqueue_scripts');
function __enqueue_scripts()
{
    if (!is_admin()) {
        global $wp_styles, $wp_scripts; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
        $version = time();

        wp_deregister_script( 'jquery' );

        wp_register_style('styles-core', get_template_directory_uri().'/library/main.min.css', [], $version);
        wp_enqueue_style('styles-core');

        // wp_enqueue_script('script-components', get_template_directory_uri().'/library/js/components.min.js', array(), false, true);
        wp_enqueue_script('script-main', get_template_directory_uri().'/library/main.min.js', array(), $version, true);
    }
}

/**
 * Add the favicons into the page header.
 * @return [type] [description]
 */
function __register_favicons() { ?>
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/library/images/favicon.png" />
<?php
/*?>
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/library/images/favicon.ico">
<?php */ }
add_action('wp_head','__register_favicons',1);

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


// Hide admin menu on non-dev environments
if (substr(getenv('APPLICATION_ENV'),0,3) != 'dev') {
    add_filter('acf/settings/show_admin', '__return_false');
}

// Theme Options Page
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'    => 'Theme Options',
        'menu_title'    => 'Theme Options',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false,
    ));
}

/**
 * Hide menus in the admin
 * @return [type] [description]
 */
function remove_menus(){

  // remove_menu_page( 'index.php' );                  //Dashboard
  // remove_menu_page( 'jetpack' );                    //Jetpack*
  remove_menu_page( 'edit.php' );                   //Posts
  // remove_menu_page( 'upload.php' );                 //Media
  // remove_menu_page( 'edit.php?post_type=page' );    //Pages
  // remove_menu_page( 'edit-comments.php' );          //Comments
  // remove_menu_page( 'themes.php' );                 //Appearance
  // remove_menu_page( 'plugins.php' );                //Plugins
  // remove_menu_page( 'users.php' );                  //Users
  // remove_menu_page( 'tools.php' );                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings

}
add_action( 'admin_menu', 'remove_menus' );


function __get_ga_tracking_id($field_name = false, $options_page = true)
{
    $production_domains = array('onpoint.studio',);

    if (in_array($_SERVER['SERVER_NAME'], $production_domains) && function_exists('get_field')) {
        $field_name = $field_name ?: 'g_a_tracking_id';
        $ga_tracking_id = get_field($field_name, $options_page ? 'option' : null);
    } else {
        $ga_tracking_id = 'UA-XXXXX-X';
    }

    return $ga_tracking_id;
}


/**
 * component_class(); Build a unique class for each component on the page.
 * @return  string
 */
if (!function_exists("component_class")) {
    function component_class($classes = array())
    {
        global $_flex_counter;
        $classes = (array) $classes;
        $classes[] = get_row_layout();
        $classes[] = "component_counter_".$_flex_counter['all'];
        $classes[] = "component_".get_row_layout()."_".$_flex_counter[get_row_layout()];

        return implode(" ",$classes);
    }
}
if (!function_exists("component_id")) {
    function component_id()
    {
        global $_flex_counter;
        $classes[] = "component_counter_".$_flex_counter['all'];

        return implode(" ",$classes);
    }
}

add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );
function your_custom_menu_item ( $items, $args ) {
    if ($args->theme_location == 'primary') {
        $items .= '<li class="top-link uk-visible-small"><a href="#top" data-uk-smooth-scroll="{offset:90}"><i class="uk-icon-arrow-up"></i></a></li>';
    }
    return $items;
}
