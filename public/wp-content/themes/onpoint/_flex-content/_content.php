<?php

/**
 * This file contains one loop to load the components into the template.
 */


if (have_rows('_content')):
    global $_flex_counter; // Used to build a unique component class.

    while (have_rows('_content')) : the_row();
        if (in_array(get_row_layout(), array(
            'hero_background',
            'text_block',
            'services',
            'two_columns',
            // 'blog_feed',
        ))) :
            @$_flex_counter['all']++;
            @$_flex_counter[get_row_layout()]++;

            if (file_exists('_flex-content/_'.get_post_type().'/'.get_row_layout())):
                get_template_part('_flex-content/_'.get_post_type().'/'.get_row_layout());
            else:
                get_template_part('_flex-content/_page/'.get_row_layout());
            endif;
        endif;
    endwhile;
endif;
