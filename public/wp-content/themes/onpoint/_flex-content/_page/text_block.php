<div id="<?php echo component_id();?>" class="<?php echo component_class('variant-'.get_sub_field('style')); ?>">
    <div class="uk-container uk-container-center">
        <div class="uk-block">
            <?php the_sub_field('text'); ?>
        </div>
    </div>
</div>
