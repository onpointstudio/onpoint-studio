<div id="<?php echo component_id();?>" class="uk-container uk-container-center <?php echo component_class(); ?>">

    <?php if (get_sub_field('title')): ?>
    <div class="title-container">
        <h2 class="title"><?php the_sub_field('title'); ?></h2>
    </div>
    <?php endif; ?>

    <div class="uk-grid uk-grid-width-1-1 uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-margin-large-bottom" data-uk-grid-margin>
    	<?php while(have_rows('services')): the_row(); ?>
        <div>
            <?php if (get_sub_field('icon')): ?>
            <div class="icon-container">
                <div class="icon">
                    <i class="uk-icon-<?php the_sub_field('icon');?> uk-icon-large"></i>
                </div>
            </div>
            <?php endif; ?>
        <?php the_sub_field('text'); ?>
        </div>
    	<?php endwhile; ?>
    </div>

</div>
