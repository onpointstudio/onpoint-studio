<div id="<?php echo component_id();?>" class="<?php echo component_class('variant-'.get_sub_field('style')); ?>">
    <div class="uk-container uk-container-center">
        <div class="uk-grid uk-grid-margin uk-grid-width-1-1 uk-grid-width-medium-1-3" data-uk-grid-margin>
            <div>
                <div class="uk-panel">
                    <img src="http://onpoint.dev/wp-content/uploads/2016/11/3xEpVqy.jpg">
                    <h3>TITLE</h3>
                    <p>Lorem Ipsum....</p>
                </div>
            </div>
            <div>
                <div class="uk-panel uk-margin">
                    <img src="http://onpoint.dev/wp-content/uploads/2016/11/3xEpVqy.jpg">
                    <h3>TITLE</h3>
                    <p>Lorem Ipsum....</p>
                </div>
            </div>
            <div>
                <div class="uk-panel uk-margin">
                    <img src="http://onpoint.dev/wp-content/uploads/2016/11/3xEpVqy.jpg">
                    <h3>TITLE</h3>
                    <p>Lorem Ipsum....</p>
                </div>
            </div>
        </div>
    </div>
</div>
