<div id="<?php echo component_id();?>" class="<?php echo component_class('variant-'.get_sub_field('style')); ?>">
    <div class="uk-container uk-container-center">
        <div class="uk-block">
            <div class="uk-grid uk-grid-width-1-1 uk-grid-width-medium-1-2">
            <?php while(have_rows('columns')): the_row();?>
                <div>
                    <div class="uk-margin">
                        <?php the_sub_field('text'); ?>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>
