<?php

/**
 * Example Post Type registration
 */
// add_action( 'init', '__register______POST_KEYWORD' );
// function __register______POST_KEYWORD() {
//
//     $plural = 'Articles';
//     $singular = 'Article';
//     $posttype = 'article'
//     $description = "";
//     $slug = 'articles';
//
// 	$labels = array(
// 		'name'               => _x( $plural, 'post type general name', TEXT_DOMAIN ),
// 		'singular_name'      => _x( ''.$singular, 'post type singular name', TEXT_DOMAIN ),
// 		'menu_name'          => _x( ''.$plural, 'admin menu', TEXT_DOMAIN ),
// 		'name_admin_bar'     => _x( ''.$singular, 'add new on admin bar', TEXT_DOMAIN ),
// 		'add_new'            => _x( 'Add New', $singular, TEXT_DOMAIN ),
// 		'add_new_item'       => __( 'Add New '.$singular, TEXT_DOMAIN ),
// 		'new_item'           => __( 'New '.$singular, TEXT_DOMAIN ),
// 		'edit_item'          => __( 'Edit '.$singular, TEXT_DOMAIN ),
// 		'view_item'          => __( 'View '.$singular, TEXT_DOMAIN ),
// 		'all_items'          => __( 'All '.$plural, TEXT_DOMAIN ),
// 		'search_items'       => __( 'Search '.$plural, TEXT_DOMAIN ),
// 		'parent_item_colon'  => __( 'Parent :.'$plural, TEXT_DOMAIN ),
// 		'not_found'          => __( 'No '.strtolower($singular).' found.', TEXT_DOMAIN ),
// 		'not_found_in_trash' => __( 'No '.strtolower($singular).' found in Trash.', TEXT_DOMAIN )
// 	);
//
// 	$args = array(
// 		'labels'             => $labels,
//         'description'        => __( $description, TEXT_DOMAIN ),
// 		'public'             => true,
// 		'publicly_queryable' => true,
// 		'show_ui'            => true,
// 		'show_in_menu'       => true,
// 		'query_var'          => true,
// 		'rewrite'            => array( 'slug' => $slug ),
// 		'capability_type'    => 'post',
// 		'has_archive'        => false,
// 		'hierarchical'       => false,
// 		'menu_position'      => null,
// 		'supports'           => array( 'title', 'thumbnail' ),
// 		// 'supports'           => array( 'title', 'editor', 'comments', 'revisions', 'trackbacks', 'author', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields', 'post-formats'),
// 	);
//
// 	register_post_type( $posttype, $args );
// }
