<?php get_header();?>

<?php if (is_404() || is_search()):?>
    <?php // TODO: Add markup to display 404 message, typically this would be the same as the TEXT_BLOCK component. ?>
    <?php the_field('404_message'); ?>
<?php else: ?>
    <?php get_template_part('_flex-content/_content'); ?>
<?php endif; ?>

<?php get_footer();?>
