<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <?php wp_head(); ?>

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', '<?php echo __get_ga_tracking_id(); ?>', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body <?php body_class(); ?> >

<header id="navigation" data-uk-sticky>
    <div class="uk-container uk-container-center">
        <a href="#" id="logo"><img src="" alt="" /> I AM THE STICKY NAV</a>

        <?php echo wp_nav_menu(array(
            // 'container' => '',
            'theme_location' => 'primary',
            'fallback_cb' => false,
            'walker' => new Menu_Walker(),
        )); ?>
    </div>
</header>

