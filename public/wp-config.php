<?php

if (getenv('APPLICATION_ENV') == 'development'):
    define('DB_NAME', 'onpoint_studio_dev');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'WrEbEsweY7xe');
    define('DB_HOST', 'smoolab-rds-001.cml2zerwfzyu.eu-west-1.rds.amazonaws.com');
    define('WP_DEBUG', true);
elseif (getenv('APPLICATION_ENV') == 'staging'):
    define('DB_NAME', 'onpoint_studio_dev');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'WrEbEsweY7xe');
    define('DB_HOST', 'smoolab-rds-001.cml2zerwfzyu.eu-west-1.rds.amazonaws.com');
    define('WP_DEBUG', true);
elseif (getenv('APPLICATION_ENV') == 'production'):
    define('DB_NAME', '');
    define('DB_USER', '');
    define('DB_PASSWORD', '');
    define('DB_HOST', 'localhost');
    define('WP_DEBUG', false);
else:
    exit("'APPLICATION_ENV has not been set!");
endif;

define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

define('AUTH_KEY',         'Iij0]^PA%aff+`vMb TxTR=c<ZC}J9uQ6No)sS(ldZg3J:9EA]K]coW<-%K=0N+!');
define('SECURE_AUTH_KEY',  '8_-Ygj_T.`aj`Ik0Rwq;1z%<f$UN*}I0?Oi`pcgGwkcQ7k.Gw1;8QGiOo-#+[o+ ');
define('LOGGED_IN_KEY',    '?h96&6:{%RLPf|&;?V$Y-.Pki-wMR44+PUt1O/PP!/Zwx:%fk4{+(Xs{f<-$[^{n');
define('NONCE_KEY',        'k#a%Jd++sPa4_{Y+k]|rC-pMfpg7|h_.blhOC(?u-VY[0S`*tH8&u%Mbq;{.})?`');
define('AUTH_SALT',        'lPq%&ym>>5H+U+Z`6i{)~_1ue{{63%0-<]zGB;*YR`MWyT!}OS=;!(-BxXAtK;}(');
define('SECURE_AUTH_SALT', ':PwJ)+X#AiK+DU[VS+3RZu`NziA|aAP4Bn]Yh|_cs-f3D3G4K4wN^jAsWA&T/WR}');
define('LOGGED_IN_SALT',   'If:csI*Z+c1$S$39$LRuLLBrYx|^f?kEW4:(Fx1[)bn;&tib^N},BOY3VJEbRT*[');
define('NONCE_SALT',       'm;d`[44D$-lqrcXOVoFQ%`F{&kDy~?9HV=x61sl)Qm{@jx(Xdu|GK$HK-|d?Lq4G');

/**#@-*/

define('WPCF7_AUTOP', false );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * Get Wordpress working with non-standard file / folder locations.
 */
define('WP_CONTENT_DIR', __DIR__ . '/wp-content');
define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');
define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp');
define('WP_HOME', 'http://' . $_SERVER['SERVER_NAME']);

/**
 * We're building sites using Composer to pull in Wordpress and the plugins - we
 * don't want updates to performed via the admin area, but rather through use
 * of composter.json.  Disable updating of plugins in WordPress Admin.
 */
define('DISALLOW_FILE_EDIT', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
